/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rulebase.h"
#include "stringutils.h"

#include <map>
#include <regex>

#include "localconsts.h"

#define registerRule(rule) \
class rule : public RuleBase \
{ \
    public: \
        rule(); \
\
        void start(); \
\
        void end(); \
\
        void parseLine(std::string data); \
}; \
\
namespace \
{ \
    rule instance##rule; \
} \

#define registerRuleExt(rule, name, ext) \
class rule : public RuleBase \
{ \
    public: \
        rule() \
        { \
            setName(name); \
            addMask(ext); \
        } \
\
        void start(); \
\
        void end(); \
\
        void parseLine(std::string data); \
}; \
\
namespace \
{ \
    rule instance##rule; \
} \


#define constructRule(rule) rule::rule()
#define startRule(rule) void rule::start()
#define endRule(rule) void rule::end()
#define parseLineRule(rule) void rule::parseLine(std::string data)
