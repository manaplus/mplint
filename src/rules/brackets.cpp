/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "template.hpp"

registerRuleExt(brackets, "015", "(.+)[.](cpp|h)")

startRule(brackets)
{
}

endRule(brackets)
{
}

parseLineRule(brackets)
{
    trim(data);
    if (!data.empty())
    {
        if (data[0] == '}')
        {
            std::string text = data.substr(1);
            trim(text);
            if (!text.empty())
            {
                if (text == "else" ||
                    text == "while")
                {
                    print("Brackets formatting error. Wrong first bracket position");
                }
            }
        }
        const size_t sz = data.size();
        if (data[data.size() - 1] == '{')
        {
            if (sz > 2)
            {
                const std::string str = data.substr(0, 2);
                if (str == "//" || str == "/*")
                    return;
            }
            std::string text = data.substr(1, data.size() - 1);
            trim(text);
            if (!text.empty())
                print("Brackets formatting error. Wrong last bracket position");
        }
    }
}
