/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "template.hpp"

bool afterCopyrights(false);
bool foundManaPlus(false);
bool checkText(false);
bool foundLicenseNumber(false);

registerRuleExt(license, "005", "(.+)[.](h|cpp|cc|inc)")

startRule(license)
{
    if (isMatch(file, "(.*)[/]((debug|sdl2gfx)[/]([^/]*)|mumblemanager"
        "|SDLMain|utils/physfsrwops|utils/base64)[.](cpp|h)"))
    {
        terminateRule();
    }
    afterCopyrights = false;
    foundManaPlus = false;
    checkText = false;
    foundLicenseNumber = false;
}

endRule(license)
{
}

parseLineRule(license)
{
    if (line == 1 && data != "/*")
    {
        print("Should be license header");
        terminateRule();
        return;
    }
    else if (line == 2 && data != " *  The ManaPlus Client")
    {
        print("Should be 'The ManaPlus Client' in header");
        terminateRule();
        return;
    }
    else if (line > 2)
    {
        if (checkText)
        {
            if (data == " */")
            {
                if (!foundLicenseNumber)
                    print("License version not found. It must be GPL 2");
                terminateRule();
            }
            else if (data == " *  the Free Software Foundation;"
                     " either version 2 of the License, or")
            {
                foundLicenseNumber = true;
                terminateRule();
            }
        }
        else if (!afterCopyrights)
        {
            if (data == " *")
            {
                if (!foundManaPlus)
                    print("Missing ManaPlus developers copyrights");
                afterCopyrights = true;
            }
            else if (strStartWith(data, " *  Copyright (C) "))
            {
                if (data.find("The ManaPlus Developers") != std::string::npos)
                    foundManaPlus = true;
            }
            else
            {
                print("Missing copyrights section");
                terminateRule();
            }
        }
        else
        {
            if (data != " *  This file is part of The ManaPlus Client.")
                print("Missing \"This file is part of The ManaPlus Client.\"");
            checkText = true;
        }
    }
}
