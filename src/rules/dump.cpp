/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "template.hpp"

/*
registerRuleExt(dumpCpp, "001", "(.+)[.]cpp")

startRule(dumpCpp)
{
    printRaw("Start checking file: " + file);
}

endRule(dumpCpp)
{
    printRaw("End checking file: " + file);
}

parseLineRule(dumpCpp)
{
    print(data);
}


registerRuleExt(dumpH, "002", "(.+)[.]h")

startRule(dumpH)
{
    printRaw("Start checking file: " + file);
}

endRule(dumpH)
{
    printRaw("End checking file: " + file);
}

parseLineRule(dumpH)
{
    print(data);
}
*/
