/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "template.hpp"

registerRuleExt(formatting, "011", "(.+)[.](cpp|h)")

namespace
{
    unsigned int align = 0;
    std::stack<unsigned int> stack;
}  // namespace

startRule(formatting)
{
    align = 0;
}

endRule(formatting)
{
}

parseLineRule(formatting)
{
    std::string data0 = data;
    trim(data0);
    if (data0 == "{")
    {
        stack.push(align);
        align = data.size() - data0.size() + 4;
//        print("open align=" + toString(align));
    }
    else if (findCutFirst(data0, "{ "))
    {
        const int sz = data0.size();
        trim(data0);
        if (!data0.empty() && data0[0] != '/')
            return;
        stack.push(align);
        align = data.size() - sz - 2 + 4;
//        print("open align=" + toString(align));
    }
    else if (data0 == "}" || data0 == "};" || findCutFirst(data0, "} "))
    {
        if (stack.empty())
            return;

        align = stack.top();
        stack.pop();
//        print("close align=" + toString(align));
    }
    else
    {
        std::string data2 = data;
        if (findCutLast(data2, "public:")
            || findCutLast(data2, "protected:")
            || findCutLast(data2, "private:"))
        {
//            print("sz=" + toString(int(data2.size())));
//            print("align=" + toString(align));
            if (data2.size() != align)
            {
                print("Wrong public/protected/private formatting. "
                    "Must be align " + toString(align) + ", but present align "
                    + toString(static_cast<int>(data2.size())) + ".");
            }
        }
    }
}
