/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "template.hpp"

registerRuleExt(constructor, "006", "(.+)[.](cpp|h)")

bool foundConstructor(false);
int align = 0;

startRule(constructor)
{
    foundConstructor = false;
    align = 0;
}

endRule(constructor)
{
}

parseLineRule(constructor)
{
    if (foundConstructor)
    {
        if (data.size() > 0 && data[0] == '#')
            return;

        int align2 = 0;
        const size_t sz = data.size();
        for (size_t f = 0; f < sz; f ++)
        {
            if (data[f] != ' ')
                break;
            align2 ++;
        }
        if (align2 != align + 4)
            print("Wrong align in initialisation list.");

        foundConstructor = false;
    }
    else
    {
        if (data.find("(") != std::string::npos &&
            data.find("A_NONNULL(") == std::string::npos)
        {
            align = 0 ;
            const size_t sz = data.size();
            for (size_t f = 0; f < sz; f ++)
            {
                if (data[f] != ' ')
                    break;
                align ++;
            }
        }
        if (strEndWith(data, "):"))
        {
            print("Add space between constructor and \":\".");
            if (data.find("<") == std::string::npos
                && data.find("?") == std::string::npos)
            {
                foundConstructor = true;
            }
        }
        else if (strEndWith(data, ") :"))
        {
            if (data.find("<") == std::string::npos
                && data.find("?") == std::string::npos)
            {
                foundConstructor = true;
            }
        }
    }
}
